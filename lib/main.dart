// ! Dependancy
import 'package:flutter/material.dart';

// ! My Screen
import 'screen/Feed.dart';
import 'screen/Welcome.dart';
import 'screen/Test.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Top Head',
      theme: ThemeData(scaffoldBackgroundColor:  Colors.indigo[100],
        appBarTheme: AppBarTheme(color: Color(0xff0B0930))
      ),
      initialRoute: '/',
      routes: {
        // '/': (context) => WelcomePage(),
        '/test': (context) => Test(),
        // '/feed': (context) => FeedPage(),
        '/': (context) => FeedPage(),
      },
    );
  }
}
