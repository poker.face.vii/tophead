import 'package:flutter/material.dart';

class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
          child: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                color: Colors.orangeAccent,
                child: Text('go to feed page'),
                onPressed: () {
                  Navigator.pushNamed(context, '/feed');
                },
              ),
              FlatButton(
                color: Colors.red,
                child: Text('go to Test'),
                onPressed: () {
                  Navigator.pushNamed(context, '/test');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
