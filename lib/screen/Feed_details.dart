import 'package:flutter/material.dart';
// ! theme 
import 'package:tophead/theme/TextStyle.dart';
class Feed_details extends StatelessWidget {
  final feed_info;
  Feed_details(this.feed_info);
  @override
  Widget build(BuildContext context) {
    return Scaffold(backgroundColor: Colors.indigo[100],
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  width: 45, child: Image.network(feed_info.publisherImg)),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Published in',
                    style: TextStyle(fontSize: 12, color: Colors.blueAccent),
                  ),
                  SizedBox(
                    height: 6,
                  ),
                  Text(
                    feed_info.publisher,
                    style: TextStyle(fontSize: 16),
                  )
                ],
              )
            ],
          ),
        ),
        body: CustomScrollView(
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildListDelegate([
                Container(
                  padding: EdgeInsets.all(12.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(feed_info.heading,
                            style: headTitleTs,
                      ),),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Container(
                              height: 30,
                              child: Image.network(feed_info.autherAvatar)),
                          SizedBox(
                            width: 5,
                          ),
                          Container(
                            child: Text(feed_info.autherFirstname + ' ' ,style: TextStyle(fontFamily: '',fontWeight: FontWeight.bold,fontSize: 15),),
                          ),
                          Container(
                            child: Text(feed_info.autherLastname,style:TextStyle(fontFamily: '',fontWeight: FontWeight.bold,fontSize: 15),)
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            child: Text(feed_info.publishDate,style:timeTs),
                          ),
                          Text(' . ',style: timeTs),
                          Container(
                            child: Text(
                              feed_info.description.length.toString() +
                                  ' min ' +
                                  'read',style: timeTs
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(feed_info.description,style: textTs,),
                      Image.network(feed_info.headImage),
                    ],
                  ),
                )
              ]),
            ),
            SliverList(
                delegate: SliverChildBuilderDelegate(
              (BuildContext contex, int index) {
                return Container(margin: EdgeInsets.all(12.0),
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(feed_info.body[index]["body"],style: textTs,),
                      Image.network(feed_info.body[index]["img"])
                    ],
                  ),
                );
              },
              childCount: feed_info.body.length,
            ))
          ],
        ));
  }
}
