// ! Dependancy
import 'dart:ffi';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

// ! screen
import 'package:tophead/screen/Feed_details.dart';
// ! MODEL
import 'package:tophead/models/Article_model.dart';
// !  Components
import 'package:tophead/components/_ListViewCart.dart';
import 'package:tophead/components/MenuDrawer.dart';
import 'package:tophead/theme/TextStyle.dart';
// !  Web Services
import 'package:tophead/web services/myAPI.dart';
import 'package:tophead/web services/my-local-var.dart';
// ----------------------------------------------------------

class FeedPage extends StatefulWidget {
  @override
  _FeedPageState createState() => _FeedPageState();
}

class _FeedPageState extends State<FeedPage> {
  Future<List<Article>> _feedData() async {
    var data = await http.get(articlesFeed);
    var jsonData = jsonDecode(data.body);
    List<Article> feeds = [];
    for (var f in jsonData) {
      Article feed = Article(
        f["id"],
        f["heading"],
        f["description"],
        f["head_image"],
        f["type"],
        f["sub_type"],
        f["auther_firstname"],
        f["auther_lastname"],
        f["auther avatar"],
        f["publisher"],
        f["publisher_img"],
        f["publish_date"],
        f["body"],
      );
      feeds.add(feed);
    }
    return feeds;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: drawerBody(),
        appBar: new AppBar(
          title: Text(app_title,style: logots,),
        ),
        body: Container(
          child: FutureBuilder(
            future: _feedData(),
            builder: (BuildContext contex, AsyncSnapshot snapshot) {
              if (snapshot.data == null) {
                return Center(
                  child: Text("loading..."),
                );
              } else {
                return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      child: listViewFeed(snapshot.data[index]),
                      onTap: () {
                        Navigator.push(
                            context,
                            new MaterialPageRoute(
                                builder: (contex) =>
                                    Feed_details(snapshot.data[index])));
                      },
                    );
                  },
                );
              }
            },
          ),
        )
        // ListView.builder(
        //   itemCount: 10,
        //   itemBuilder: (BuildContext context, int index) {
        //     return _listViewFeed();
        //   },
        // ),
        );
  }
}


