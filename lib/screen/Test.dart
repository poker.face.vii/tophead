import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

// class Artic {
//   final String head;
//   final String description;
//   final String readTime;

//   Artic(this.head,this.description,this.readTime);
// }
class User {
  final int age;
  final String name;
  final String email;
  // final String img;

  User(this.age, this.name, this.email, );
}

class Test extends StatefulWidget {
  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<Test> {
  // final myArtic = new Artic('head123', 'description', 9);
  // final nsp = 2;
  // List<Artic> articles = [];
  // Future<List<Artic>> _articles() async{
  //   var  data = await http.get("https://swapi.co/api/people/1");
  //   var jsonData = json.decode(data.body);
  //   for (var bookval in jsonData){
  //     Artic artic = Artic(bookval['name'], bookval['height'], bookval['mass']);
  //     articles.add(artic);
  //   }
  //   return articles;
  // }

  Future<List<User>> _getUsers() async {
    var data = await http
        .get("https://tophead.free.beeceptor.com/headfeed");
    var jsonData = json.decode(data.body);
    List<User> users = [];
    for (var u in jsonData) {
      User user = User(u["id"], u["head"], u["description"]);
      users.add(user);
    }
    print(users.length);
    return users;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        child: FutureBuilder(
          future: _getUsers(),
          builder: (BuildContext contex, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Container(
                child: Center(
                  child: Text('loading...'),
                ),
              );
            } else {
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext contex, int index) {
                  return Card(
                      child: GestureDetector(
                    child: Column(
                      children: <Widget>[
                        Text(snapshot.data[index].name),
                        Text(snapshot.data[index].age.toString()),
                        Text(snapshot.data[index].email),
                        
                      ],
                    ),
                    onTap: () {
                      Navigator.push(
                          context, new MaterialPageRoute(builder: (contex) => UserDetails(snapshot.data[index])));
                    },
                  ));
                },
              );
            }
          },
        ),
      ),
    );
  }
}

class UserDetails extends StatelessWidget {
  final User user;
  UserDetails(this.user);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(user.name),
      ),
    );
  }
}
