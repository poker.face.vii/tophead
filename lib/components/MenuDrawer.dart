import 'package:flutter/material.dart';
// ! Components 
import 'package:tophead/components/AvatarImage.dart';

Widget drawerBody() {
  return Drawer(
    child: Container(
      color: Colors.grey[100],
      child: ListView(
        children: <Widget>[
          Container(
            color: Colors.white,
            padding: const EdgeInsets.only(
                top: 12.0, left: 25, right: 1.0, bottom: 5.0),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    avatarImg('images/avatar6.png', 90, 90),
                  ],
                ),
                FlatButton(
                  onPressed: () {},
                  child: Container(
                    child: Text('Adam Lambert'),
                    alignment: Alignment.topLeft,
                  ),
                ),
                Container(
                    height: 20,
                    child: FlatButton(
                      onPressed: () {},
                      child: Container(
                        child: Text(
                          'See profile',
                          style: TextStyle(color: Colors.black54, fontSize: 12),
                        ),
                        alignment: Alignment.bottomLeft,
                      ),
                    ))
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.only(
                top: 12.0, left: 25, right: 1.0, bottom: 5.0),
            child: Column(
              children: <Widget>[
                FlatButton(
                  onPressed: () {},
                  child: Container(
                    child: Text('Home'),
                    alignment: Alignment.topLeft,
                  ),
                ),
                FlatButton(
                  onPressed: () {},
                  child: Container(
                    child: Text('Bookmark'),
                    alignment: Alignment.topLeft,
                  ),
                ),
                FlatButton(
                  onPressed: () {},
                  child: Container(
                    child: Text('Intrests'),
                    alignment: Alignment.topLeft,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    ),
  );
}