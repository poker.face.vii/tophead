import 'package:flutter/material.dart';


Widget avatarImg(img, heigth, width) {
  return ClipRRect(
    borderRadius: BorderRadius.circular(50),
    child: Container(
      child: Align(
        alignment: Alignment.topLeft,
        child: Image(
            width: heigth + .0, height: width + .0, image: AssetImage(img)),
      ),
    ),
  );
}
