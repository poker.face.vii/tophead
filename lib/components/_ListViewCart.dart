import 'package:flutter/material.dart';

// !  theme
import 'package:tophead/theme/TextStyle.dart';

Widget listViewFeed(feed_info) {
  return Card(
    child: Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                feed_info.heading,
                style: headTitleTs,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0, left: 2),
                child: Text(
                  feed_info.description,
                  style: descripTs,
                ),
              ),
              SizedBox(
                height: 18,
              ),
            _typeGroup(feed_info),
              _autherAndPublisher(feed_info),
              _dateAndTime(feed_info),
            ],
          ),
          Column(
            children: <Widget>[
              _imagePart(feed_info),
            ],
          )
        ],
      ),
    ),
  );
}



Widget _autherAndPublisher(feed_info) {
  return Container(
    padding: EdgeInsets.only(top: 4.5, left: 2),
    child: RichText(
      text: TextSpan(
          text: feed_info.autherFirstname + ' ',
          style: TextStyle(
              color: Color(0xff000000),
              fontSize: 12,
              fontFamily: paragraphFont,
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.w600),
          children: <TextSpan>[
            TextSpan(text: feed_info.autherLastname),
            TextSpan(text: ' in ', style: TextStyle(color: Color(0xff6A7070))),
            TextSpan(text: feed_info.publisher)
          ]),
    ),
  );
}



Widget _typeGroup(feed_info){
  return    Row(
                children: <Widget>[
                  Container(
                    decoration: new BoxDecoration(
                        color: Color(0xff236776),
                        borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(4.0),
                          bottomLeft: const Radius.circular(4.0),
                        )),
                    padding: EdgeInsets.all(3),
                    child: Text(feed_info.type, style: typeTs),
                  ),
                  Container(
                    decoration: new BoxDecoration(
                        color: Color(0xff347623),
                        borderRadius: new BorderRadius.only(
                          topRight: const Radius.circular(4.0),
                          bottomRight: const Radius.circular(4.0),
                        )),
                    padding: EdgeInsets.all(3),
                    child: Text(feed_info.subType, style: typeTs),
                  ),
                ],
              );
}


Widget _dateAndTime(feed_info) {
  return Container(
                padding: EdgeInsets.only(top: 4.5, left: 2),
                child: RichText(
                  text: TextSpan(
                      text: feed_info.publishDate + ' ',
                      style: TextStyle(color: Color(0xff6A7070), fontSize: 12,),
                      children: <TextSpan>[
                        TextSpan(
                            text: feed_info.description.length.toString() +
                                ' min ',
                            style: TextStyle(color: Color(0xff6A7070),fontFamily: infoFont,fontWeight: FontWeight.bold)),
                        TextSpan(text: 'Read')
                      ]),
                ),
              );
}



Widget _imagePart(feed_info) {
  return Stack(
    children: <Widget>[
      Container(
        height: 85,
        width: 85,
        color: Colors.white,
      ),
      Positioned.fill(
        child: Align(
          alignment: Alignment.bottomLeft,
          child: Container(
            height: 80,
            width: 80,
            child: Image.network(feed_info.headImage),
            color: Colors.grey,
          ),
        ),
      ),
      Positioned.fill(
        child: Align(
          alignment: Alignment.topRight,
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(width: 2.5, color: Colors.white),
              color: Colors.blue,
            ),
            height: 25,
            width: 25,
            child: Image.network(feed_info.publisherImg),
          ),
        ),
      )
    ],
  );
}
