class Article {
  final int id;
  final String heading;
  final String description;
  final String headImage;
  final String type;
  final String subType;
  final String autherFirstname;
  final String autherLastname;
  final String autherAvatar;
  final String publisher;
  final String publisherImg;
  final String publishDate;
  final List body;

  Article(
      this.id,
      this.heading,
      this.description,
      this.headImage,
      this.type,
      this.subType,
      this.autherFirstname,
      this.autherLastname,
      this.autherAvatar,
      this.publisher,
      this.publisherImg,
      this.publishDate,
      this.body);
}