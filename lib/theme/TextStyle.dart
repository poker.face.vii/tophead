import 'package:flutter/material.dart';

// *  my fonts

final String headFont = 'Blinker';
final String paragraphFont = 'RobotoCondensed';
final String infoFont = 'BigSDisplay';
final String logoFont = 'Odibee';
final String stylishFont = 'FugazOne';

// * Text style
final TextStyle logots = TextStyle(fontFamily: logoFont);

final TextStyle typeTs = TextStyle(
  color: Colors.white,
  fontFamily: infoFont,fontWeight: FontWeight.w700
);
final TextStyle headTitleTs =
    TextStyle(fontFamily: headFont, fontSize: 24, fontWeight: FontWeight.w600);

final TextStyle descripTs =
    TextStyle(fontFamily: stylishFont, fontSize: 15, color: Color(0xff6A7070));
final TextStyle textTs =
    TextStyle(fontFamily: paragraphFont,fontWeight: FontWeight.w600,fontSize: 18);
final TextStyle timeTs =
  TextStyle(fontFamily: infoFont ,fontSize: 14,fontWeight: FontWeight.w600);


    
